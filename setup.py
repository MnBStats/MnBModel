from setuptools import setup, find_packages

setup(
    name='mnbmodel',
    version='0.1.0.dev',
    author="Piotr Wilk",
    packages=find_packages(exclude=['test*']),
    py_modules=['config'],
    license='',
    long_description=open('README.txt').read(),
    test_suite='tests'
)
