import unittest
from mnbmodel.db import Database
from mnbmodel.query import add
from mnbmodel.transactions.assign import assign_to_team

TOUR_FULL_NAME = "SuckItDownLeague"
TOURNAMENT_NAME = "SIDL"
HOME_TEAM_NAME = "Tribe"
AWAY_TEAM_NAME = "Squad"
HOME_TAG = "T_"
AWAY_TAG = "S_"
KILLER_GAME_ID = 1
VICTIM_GAME_ID = 100


class DatabaseTestSuite(unittest.TestCase):
    def setUp(self):
        self.db = Database(conn_string='sqlite:///:memory:')
        self.session = self.db.connect()

    def tearDown(self):
        self.session.rollback()
        self.session.close()


def setup_tournament(session):
    tour = add.tournament(session, TOURNAMENT_NAME, TOUR_FULL_NAME)
    home_team = add.team(session, HOME_TEAM_NAME, HOME_TAG, TOURNAMENT_NAME)
    away_team = add.team(session, AWAY_TEAM_NAME, AWAY_TAG, TOURNAMENT_NAME)
    match = add.match(session, home_team, away_team,
                      week_number=1, home_score=9, away_score=7)

    killer = assign_to_team(
        session,
        name="Killer",
        game_id=KILLER_GAME_ID,
        tag=HOME_TAG,
        tournament_name=TOURNAMENT_NAME)

    enemy = assign_to_team(
        session,
        name="Enemy",
        game_id=VICTIM_GAME_ID,
        tag=AWAY_TAG,
        tournament_name=TOURNAMENT_NAME)

    return tour, home_team, away_team, match, killer, enemy


def add_headshot(session, match, killer, victim, *, round):
    add.kill(
        session,
        match=match,
        killer=killer,
        victim=victim,
        kill_type="<img=ico_headshot>",
        spawn=1,
        round=round,
        kill_number=1,
        weapon_id='<weapon=Crossbow>')


def add_kill(session, match, killer, victim, *, round, spawn=1,
             kill_number=1, weapon='<weapon=Scimitar>'):
    add.kill(
        session,
        match=match,
        killer=killer,
        victim=victim,
        kill_type="SomeKillType",
        spawn=spawn,
        round=round,
        kill_number=kill_number,
        weapon_id=weapon)


def add_assist(session, match, player, round):
    add.assist(
        session,
        player=player,
        match=match,
        dmg=60,
        team_dmg=0,
        num_of_assists=1,
        spawn_number=1,
        round_number=round)


def add_match_player(session, match, player, spawn, round, cls='Archer'):
    add.match_player(
        session,
        match=match,
        player=player,
        side=0,
        wb_class=cls,
        spawn=spawn,
        round=round)
