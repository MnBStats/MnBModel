import unittest
from mnbmodel.query import add
from mnbmodel.transactions import single_stats
from mnbmodel.transactions.util.player_entry import PlayerEntry, PlayerEntries
from mnbmodel.transactions.util.filters import match_filter, team_filter

from .database_test_base import \
    DatabaseTestSuite, setup_tournament, add_headshot, TOURNAMENT_NAME


class HeadshotTestSuite(DatabaseTestSuite):
    def setUp(self):
        """
        Database state:
            Match 1:
            killer - two headshots
            enemy - one headshot

            Match 2:
            killer - one headshot
        """
        DatabaseTestSuite.setUp(self)
        tour, home, away, match, killer, enemy = setup_tournament(self.session)
        second_match = add.match(self.session, away, home,
                                 week_number=2, home_score=9, away_score=7)

        add_headshot(self.session, match, killer, enemy, round=1)
        add_headshot(self.session, match, killer, enemy, round=2)
        add_headshot(self.session, match, enemy, killer, round=3)
        add_headshot(self.session, second_match, killer, enemy, round=1)
        self.match = match
        self.home = home

    def test_select_headshots_from_whole_tournament(self):
        """
        Expected:
            name     K
            Killer   3
            Enemy    1
        """
        expected = PlayerEntries([])
        expected.entries.append(PlayerEntry(name="Killer", K=3))
        expected.entries.append(PlayerEntry(name="Enemy", K=1))

        result = single_stats.headshots(self.session, TOURNAMENT_NAME)
        self.assertEqual(expected, result)

    def test_limit_the_results_when_limit_given(self):
        """
        Limit = 1
        Expected:
            name     K
            Killer   3
        """
        expected = PlayerEntries([])
        expected.entries.append(PlayerEntry(name="Killer", K=3))

        result = single_stats.headshots(self.session, TOURNAMENT_NAME, limit=1)
        self.assertEqual(expected, result)

    def test_select_hs_from_one_match_when_match_filter_passed(self):
        """
        Filters - match_filter
        Expected:
            name     K
            Killer   2
            Enemy    1
        """
        expected = PlayerEntries([])
        expected.entries.append(PlayerEntry(name="Killer", K=2))
        expected.entries.append(PlayerEntry(name="Enemy", K=1))

        result = single_stats.headshots(
            self.session, TOURNAMENT_NAME, filters=[match_filter(self.match)])
        self.assertEqual(expected, result)

    def test_select_hs_from_one_team_when_team_filter_passed(self):
        """
        Filters - team_filter
        Expected:
            name     K
            Killer   3
        """
        expected = PlayerEntries([])
        expected.entries.append(PlayerEntry(name="Killer", K=3))

        result = single_stats.headshots(
            self.session, TOURNAMENT_NAME, filters=[team_filter(self.home)])
        self.assertEqual(expected, result)

    def test_select_hs_from_one_match_and_team_when_filters_passed(self):
        """
        Filters - match_filter, team_filter
        Expected:
            name     K
            Killer   2
        """
        expected = PlayerEntries([])
        expected.entries.append(PlayerEntry(name="Killer", K=2))

        result = single_stats.headshots(
            self.session,
            TOURNAMENT_NAME,
            filters=[match_filter(self.match), team_filter(self.home)])

        self.assertEqual(expected, result)


if __name__ == '__main__':
    unittest.main()
