import unittest
from mnbmodel.query import add
from mnbmodel.transactions.assign import assign_to_team
from mnbmodel.transactions.match_class_chart import KillsPerClass, \
    read_kills_per_class, RANGED_CLASSES, INFANTRY_CLASSES, CAVALRY_CLASSES
from .database_test_base import DatabaseTestSuite, setup_tournament, \
    add_match_player, add_kill, TOURNAMENT_NAME, HOME_TAG

unittest.TestCase.maxDiff = None


class DeathsOrderChartTestSuite(DatabaseTestSuite):
    def setUp(self):
        DatabaseTestSuite.setUp(self)

    def test_count_kills_per_class(self):
        _, home, _, match, ranger, enemy = setup_tournament(self.session)

        infantry = assign_to_team(
            self.session,
            name="Infantry",
            game_id=ranger.game_ids[0].game_id + 1,
            tag=HOME_TAG,
            tournament_name=TOURNAMENT_NAME)

        cavalry = assign_to_team(
            self.session,
            name="Cavalry",
            game_id=ranger.game_ids[0].game_id + 2,
            tag=HOME_TAG,
            tournament_name=TOURNAMENT_NAME)

        add_match_player(self.session, match, ranger, spawn=1, round=1, cls=RANGED_CLASSES[0])
        add_match_player(self.session, match, infantry, spawn=1, round=1, cls=INFANTRY_CLASSES[0])
        add_match_player(self.session, match, cavalry, spawn=1, round=1, cls=CAVALRY_CLASSES[0])
        add_match_player(self.session, match, enemy, spawn=1, round=1)
        add_kill(self.session, match, ranger, enemy, spawn=1, round=1, weapon='bow')
        add_kill(self.session, match, infantry, enemy, spawn=1, round=1, kill_number=2, weapon='sword')
        add_kill(self.session, match, cavalry, enemy, spawn=1, round=1, kill_number=3, weapon='lance')

        expected = KillsPerClass(
            inf=[('sword', 1)],
            rng=[('bow', 1)],
            cav=[('lance', 1)])
        result = read_kills_per_class(self.session, match, home)

        self.assertEqual(expected, result)

    def test_kills_from_other_matches_does_not_count(self):
        _, home, away, match, player, enemy = setup_tournament(self.session)
        other_match = add.match(self.session, home, away, week_number=3, home_score=8, away_score=8)
        add_match_player(self.session, other_match, player, spawn=1, round=1, cls=RANGED_CLASSES[0])
        add_match_player(self.session, other_match, enemy, spawn=1, round=1, cls=INFANTRY_CLASSES[0])
        add_kill(self.session, other_match, player, enemy, spawn=1, round=1)

        expected = KillsPerClass([], [], [])
        result = read_kills_per_class(self.session, match, home)

        self.assertEqual(expected, result)


if __name__ == '__main__':
    unittest.main()