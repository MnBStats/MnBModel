import unittest

import mnbmodel.query.read
from . import database_test_base as base
from mnbmodel import models as m
from mnbmodel.transactions.prepare_match import prepare_match
from mnbmodel.query import add


TOUR_FULL_NAME = "XaXaXa"
TOURNAMENT_NAME = "XXX"
HOME_TEAM_NAME = "Tribe"
AWAY_TEAM_NAME = "EloTeam"
HOME_TAG = "T_"
AWAY_TAG = "ET_"
RANDOM_TAG = "XOXO_"
WEEK_NUMBER = 99


class PrepareMatchTestSuite(base.DatabaseTestSuite):
    def setUp(self):
        base.DatabaseTestSuite.setUp(self)

        add.tournament(self.session, TOURNAMENT_NAME, TOUR_FULL_NAME)
        add.team(self.session, HOME_TEAM_NAME, HOME_TAG, TOURNAMENT_NAME)
        add.team(self.session, AWAY_TEAM_NAME, AWAY_TAG, TOURNAMENT_NAME)

    def expect_exception(self, exception, home_tag, away_tag):
        with self.assertRaises(exception):
            prepare_match(
                self.session, TOURNAMENT_NAME, WEEK_NUMBER, home_tag, away_tag,
                home_score=8, away_score=8)

    def prepare_default_match(self):
        prepare_match(
            self.session, TOURNAMENT_NAME, WEEK_NUMBER, HOME_TAG, AWAY_TAG,
            home_score=8, away_score=8)

    def add_kill(self, match_id):
        self.session.add(m.Kill(
            match_id=match_id,
            killer_id=1,
            victim_id=2,
            kill_type='Type1',
            spawn=1,
            round=1,
            kill_number=1,
            teamkill=0))

    def add_assist(self, match_id):
        self.session.add(m.Assist(
            match_id=match_id,
            player_id=1,
            dmg=180,
            team_dmg=60,
            number_of_assists=5,
            spawn=1,
            round=1))

    def add_spawned_player(self, match_id):
        self.session.add(m.MatchPlayer(
            match_id=match_id,
            player_id=1,
            side=0,
            wb_class='Horseman',
            spawn=1,
            round=1))

    def test_raise_exception_when_team_does_not_exist(self):
        """
        Teams with given tags must be in database.
        Otherwise exception is raised.
        """
        self.expect_exception(mnbmodel.query.read.TeamDoesNotExistError, HOME_TAG, RANDOM_TAG)
        self.expect_exception(mnbmodel.query.read.TeamDoesNotExistError, RANDOM_TAG, AWAY_TAG)

    def test_add_new_match(self):
        """
        Add new match when there is no match for given week number and teams.
        """
        prepare_match(
            self.session, TOURNAMENT_NAME, WEEK_NUMBER, HOME_TAG, AWAY_TAG,
            home_score=8, away_score=8)

        matches = self.session.query(m.Match).all()
        self.assertEqual(1, len(matches))

    def test_clear_kill_when_adding_existing_match(self):
        self.prepare_default_match()

        match = self.session.query(m.Match).first()
        self.add_kill(match.match_id)
        self.prepare_default_match()

        self.assertEqual(0, len(self.session.query(m.Kill).all()))

    def test_clear_assist_when_adding_existing_match(self):
        self.prepare_default_match()

        match = self.session.query(m.Match).first()
        self.add_assist(match.match_id)
        self.prepare_default_match()

        self.assertEqual(0, len(self.session.query(m.Assist).all()))

    def test_clear_spawned_player_when_adding_existing_match(self):
        self.prepare_default_match()

        match = self.session.query(m.Match).first()
        self.add_spawned_player(match.match_id)
        self.prepare_default_match()

        self.assertEqual(0, len(self.session.query(m.MatchPlayer).all()))


if __name__ == '__main__':
    unittest.main()
