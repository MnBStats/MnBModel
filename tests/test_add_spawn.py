import unittest
from mnbmodel.query import *
from mnbmodel.models import Spawn
from . import database_test_base as base

MAP = "Oasis"
FAC1 = "Kingdom of Vaegirs"
FAC2 = "Kingdom of Nords"


class AddSpawnTestSuite(base.DatabaseTestSuite):
    def setUp(self):
        base.DatabaseTestSuite.setUp(self)

    def test_add_spawm(self):
        tour, home, away, match, killer, enemy = base.setup_tournament(self.session)
        add.spawn(self.session, match=match, spawn_number=3, map=MAP,
                  a_fac=FAC1, a_team=home.tournament_team_id, a_score=3,
                  d_score=0, d_team=away.tournament_team_id, d_fac=FAC2, start_time="19:30:00")
        spawn = self.session.query(Spawn).one()
        self.assertEqual(match.match_id, spawn.match_id)
        self.assertEqual(MAP, spawn.map)
        self.assertEqual(FAC1, spawn.attacking_faction)
        self.assertEqual(FAC2, spawn.defending_faction)


if __name__ == '__main__':
    unittest.main()
