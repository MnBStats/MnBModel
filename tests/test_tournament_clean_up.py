import unittest

from mnbmodel import models as m
from mnbmodel.query import add, delete
from mnbmodel.transactions.prepare_match import prepare_match
from . import database_test_base as base

TOUR_FULL_NAME = "XaXaXa"
TOURNAMENT_NAME = "XXX"
HOME_TEAM_NAME = "Tribe"
AWAY_TEAM_NAME = "EloTeam"
HOME_TAG = "T_"
AWAY_TAG = "ET_"
RANDOM_TAG = "XOXO_"
WEEK_NUMBER = 99


class TournamentCleanUpTestSuite(base.DatabaseTestSuite):
    def setUp(self):
        base.DatabaseTestSuite.setUp(self)
        add.tournament(self.session, TOURNAMENT_NAME, TOUR_FULL_NAME)
        add.team(self.session, HOME_TEAM_NAME, HOME_TAG, TOURNAMENT_NAME)
        add.team(self.session, AWAY_TEAM_NAME, AWAY_TAG, TOURNAMENT_NAME)
        prepare_match(self.session, TOURNAMENT_NAME, WEEK_NUMBER, HOME_TAG,
                      AWAY_TAG, home_score=9, away_score=7)

    def test_when_removing_tournament_romove_matches_and_teams_aswell(self):
        delete.tournament(self.session, TOURNAMENT_NAME)
        self.assertEqual(0, len(self.session.query(m.Match).all()))


if __name__ == '__main__':
    unittest.main()
