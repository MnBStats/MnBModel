import unittest
from mnbmodel.transactions.assign import assign_to_team
from mnbmodel.transactions.deaths_order_chart import \
    read_deaths_order_in_match, DeathInfo

from .database_test_base import \
    DatabaseTestSuite, setup_tournament, add_match_player, add_kill, HOME_TAG, \
    TOURNAMENT_NAME, KILLER_GAME_ID

unittest.TestCase.maxDiff = None

FIRST_ROUND = 0
SECOND_ROUND = 1
THIRD_ROUND = 2


class DeathsOrderChartTestSuite(DatabaseTestSuite):
    def setUp(self):
        DatabaseTestSuite.setUp(self)

    def test_multiple_players_multiple_spawns_no_deaths(self):
        _, home, _, match, player, _ = setup_tournament(self.session)

        friend = assign_to_team(
            self.session,
            name="Friend",
            game_id=KILLER_GAME_ID+1,
            tag=HOME_TAG,
            tournament_name=TOURNAMENT_NAME)

        add_match_player(self.session, match, player, spawn=1, round=1)
        add_match_player(self.session, match, player, spawn=1, round=2)
        add_match_player(self.session, match, player, spawn=2, round=1)
        add_match_player(self.session, match, friend, spawn=2, round=1)
        survived = 3  # max number of players in round + 1

        result = read_deaths_order_in_match(self.session, match, home, survived)
        self.assertEqual(survived, result['Killer'][FIRST_ROUND].death_number)
        self.assertEqual(survived, result['Killer'][SECOND_ROUND].death_number)
        self.assertEqual(survived, result['Killer'][THIRD_ROUND].death_number)
        self.assertEqual(survived, result['Friend'][THIRD_ROUND].death_number)

    def test_multiple_players_with_deaths(self):
        """
        Two players in a match:
            player - with cls=I and weapon=sword
            friend - with cls=A and weapon=bow

        Test DeathInfo is correctly fetched in multiple rounds.
        :return:
        """
        _, home, _, match, player, _ = setup_tournament(self.session)

        friend = assign_to_team(
            self.session,
            name="Friend",
            game_id=KILLER_GAME_ID+1,
            tag=HOME_TAG,
            tournament_name=TOURNAMENT_NAME)

        add_match_player(self.session, match, player, spawn=1, round=1, cls='I')
        add_match_player(self.session, match, player, spawn=1, round=2, cls='I')
        add_match_player(self.session, match, friend, spawn=1, round=2, cls='A')
        add_match_player(self.session, match, player, spawn=2, round=1, cls='I')
        add_match_player(self.session, match, friend, spawn=2, round=1, cls='A')

        add_kill(self.session, match, player, friend, spawn=1, round=2, kill_number=1, weapon='sword')
        add_kill(self.session, match, friend, player, spawn=1, round=2, kill_number=2, weapon='bow')
        add_kill(self.session, match, friend, player, spawn=2, round=1, kill_number=1, weapon='bow')
        add_kill(self.session, match, player, friend, spawn=2, round=1, kill_number=2, weapon='sword')
        survived = 3  # max number of players in round + 1

        expected = {
            'Killer': [
                DeathInfo(survived, cls='I', weapon=None, enemy_cls=None),
                DeathInfo(death_number=2, cls='I', weapon='bow', enemy_cls='A'),
                DeathInfo(death_number=1, cls='I', weapon='bow', enemy_cls='A')
            ],
            'Friend': [
                DeathInfo(None),
                DeathInfo(death_number=1, cls='A', weapon='sword', enemy_cls='I'),
                DeathInfo(death_number=2, cls='A', weapon='sword', enemy_cls='I')
            ]
        }

        result = read_deaths_order_in_match(self.session, match, home, survived)
        self.assertEqual(expected, result)


if __name__ == '__main__':
    unittest.main()
