import unittest
from mnbmodel.query import add, read
from mnbmodel.transactions.assign import assign_to_team, MulticlanningError
from mnbmodel.query.read import TeamDoesNotExistError
from . import database_test_base as base

TOUR_FULL_NAME = "XaXaXa"
TOURNAMENT_NAME = "XXX"
TEAM_NAME = "Tribe_"
TEAM_TAG = "T_"
GAME_ID = 7
NAME = "Dembele"


class AssignPlayerTestSuite(base.DatabaseTestSuite):
    def setUp(self):
        base.DatabaseTestSuite.setUp(self)

        add.tournament(self.session, TOURNAMENT_NAME, TOUR_FULL_NAME)
        add.team(self.session, TEAM_NAME, TEAM_TAG, TOURNAMENT_NAME)
        self.session.commit()

    def test_raise_exception_when_team_does_not_exist(self):
        with self.assertRaises(TeamDoesNotExistError):
            assign_to_team(
                self.session, NAME, GAME_ID, "WEIRD_TAG_", TOURNAMENT_NAME)

    def test_when_trying_to_assign_new_player_create_player_and_assign(self):
        """
        When player does not exist in database, create new player with given
        name and game_id.
        Assign him to given team.
        """
        assign_to_team(self.session, NAME, GAME_ID, TEAM_TAG, TOURNAMENT_NAME)

        player = read.player(self.session, GAME_ID)
        self.assertEqual([GAME_ID], [obj.game_id for obj in player.game_ids])
        self.assertEqual(NAME, player.name)
        self.assertEqual(TEAM_TAG, player.teams[0].team_tag)

    def test_assign_player_when_hes_not_assign_to_any_team(self):
        """
        When player is already in database, assign him to given team.
        Do not update Names table even if the given name is different.
        """
        add.player(self.session, name=NAME, game_id=GAME_ID)

        assign_to_team(
            self.session, "DifferentName", GAME_ID, TEAM_TAG, TOURNAMENT_NAME)

        player = read.player(self.session, GAME_ID)
        self.assertEqual([GAME_ID], [obj.game_id for obj in player.game_ids])
        self.assertEqual(NAME, player.name)
        self.assertEqual(TEAM_TAG, player.teams[0].team_tag)

    def test_assign_player_second_time_to_the_same_team_has_no_effect(self):
        """
        Assigning player to the same time for the second time has no effect
        on db.
        """
        assign_to_team(self.session, NAME, GAME_ID, TEAM_TAG, TOURNAMENT_NAME)
        assign_to_team(self.session, NAME, GAME_ID, TEAM_TAG, TOURNAMENT_NAME)

        player = read.player(self.session, GAME_ID)
        self.assertEqual([GAME_ID], [obj.game_id for obj in player.game_ids])
        self.assertEqual(NAME,  player.name)
        self.assertEqual(TEAM_TAG, player.teams[0].team_tag)
        self.assertEqual(1, len(player.teams))

    def test_raise_exception_when_player_is_already_assign_in_other_team(self):
        """
        Player cannot be assign to more than one team per tournament.
        Raise multiclanning exception at attempt to assign to his second team.
        """
        assign_to_team(self.session, NAME, GAME_ID, TEAM_TAG, TOURNAMENT_NAME)

        _team = add.team(self.session, "Cuckolds", "C_", TOURNAMENT_NAME)

        with self.assertRaises(MulticlanningError):
            assign_to_team(
                self.session, NAME, GAME_ID, _team.team_tag, TOURNAMENT_NAME)


if __name__ == '__main__':
    unittest.main()
