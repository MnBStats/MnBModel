import unittest
from mnbmodel.query import add
from mnbmodel.transactions import general_stats as g
from mnbmodel.transactions.assign import assign_to_team
from mnbmodel.transactions.util.player_entry import PlayerEntry, PlayerEntries

from .database_test_base import \
    DatabaseTestSuite, setup_tournament, add_kill, add_assist, \
    TOURNAMENT_NAME, KILLER_GAME_ID, HOME_TAG


class GeneralStatsTestSuite(DatabaseTestSuite):
    def setUp(self):
        DatabaseTestSuite.setUp(self)

    def test_select_kills_for_general_stats(self):
        tour, _, _, match, killer, enemy = setup_tournament(self.session)

        add_kill(self.session, match, killer, enemy, round=1)
        add_kill(self.session, match, killer, enemy, round=2)

        stats = g.general_stats(self.session, tour.tournament_name)
        self.assertEqual(PlayerEntry(name="Killer", K=2, D=0, TK=0, A=0), stats[0])
        self.assertEqual(PlayerEntry(name="Enemy", K=0, D=2, TK=0, A=0), stats[1])

    def test_select_tks_for_general_stats(self):
        tour, _, _, match, killer, _ = setup_tournament(self.session)

        tked = assign_to_team(
            self.session,
            name="TKed",
            game_id=KILLER_GAME_ID+1,
            tag=HOME_TAG,
            tournament_name=TOURNAMENT_NAME)

        add_kill(self.session, match, killer, tked, round=1)
        add_kill(self.session, match, killer, tked, round=2)

        stats = g.general_stats(self.session, tour.tournament_name)

        self.assertEqual(PlayerEntry(name="TKed", K=0, D=2, TK=0, A=0), stats[0])
        self.assertEqual(PlayerEntry(name="Killer", K=0, D=0, TK=2, A=0), stats[1])

    def test_select_assists_for_general_stats(self):
        tour, _, _, match, player, enemy = setup_tournament(self.session)

        add_assist(self.session, match, player, round=1)
        add_assist(self.session, match, player, round=2)
        add_kill(self.session, match, player, enemy, round=1)

        stats = g.general_stats(self.session, tour.tournament_name)
        expected = PlayerEntry(name="Killer", K=1, D=0, TK=0, A=2)
        self.assertEqual(expected, stats[0])


class TeamStatsTestSuite(DatabaseTestSuite):
    def setUp(self):
        DatabaseTestSuite.setUp(self)

    def test_select_kills_only_given_team_players(self):
        tour, team, _, match, killer, enemy = setup_tournament(self.session)

        add_kill(self.session, match, killer, enemy, round=1)
        add_kill(self.session, match, killer, enemy, round=2)

        expected = PlayerEntries([])
        expected.entries.append(PlayerEntry(name="Killer", K=2, D=0, TK=0, A=0))

        stats = g.team_stats(self.session, match, team)

        self.assertEqual(expected, stats)

    def test_select_deaths_only_given_team_players(self):
        tour, team, _, match, victim, enemy = setup_tournament(self.session)

        add_kill(self.session, match, enemy, victim, round=1)
        add_kill(self.session, match, enemy, victim, round=2)

        expected = PlayerEntries([])
        expected.entries.append(
            PlayerEntry(name=victim.name, K=0, D=2, TK=0, A=0))

        stats = g.team_stats(self.session, match, team)

        self.assertEqual(expected, stats)

    def test_select_assists_only_given_team_players(self):
        tour, team, _, match, killer, enemy = setup_tournament(self.session)

        add_assist(self.session, match, killer, round=1)
        add_assist(self.session, match, enemy, round=1)
        add_kill(self.session, match, killer, enemy, round=1)

        expected = PlayerEntries([])
        expected.entries.append(PlayerEntry(name="Killer", K=1, D=0, TK=0, A=1))

        stats = g.team_stats(self.session, match, team)

        self.assertEqual(expected, stats)

    def test_select_only_from_given_match(self):
        tour, home, away, match, killer, enemy = setup_tournament(self.session)
        second_match = add.match(self.session, home, away,
                                 week_number=2, home_score=8, away_score=8)

        add_assist(self.session, match, killer, round=1)
        add_assist(self.session, second_match, killer, round=1)
        add_kill(self.session, match, killer, enemy, round=1)

        expected = PlayerEntries([])
        expected.entries.append(PlayerEntry(name="Killer", K=1, D=0, TK=0, A=1))

        stats = g.team_stats(self.session, match, home)

        self.assertEqual(expected, stats)


class StarOfTheWeekTestSuite(DatabaseTestSuite):
    def setUp(self):
        DatabaseTestSuite.setUp(self)

    def test_select_star_with_priorities_K_D_TK_A(self):
        """
        Entries:
            Star       - K=1, D=0, TK=0, A=1
            Challanger - K=1, D=0, TK=0, A=0
            Victim     - K=1, D=1, TK=0, A=0
            Teamkiller - K=1, D=0, TK=1, A=0
            Enemy      - K=0, D=!, TK=0, A=0
        """
        tour, home, away, match, star, enemy = setup_tournament(self.session)

        challanger = assign_to_team(
            self.session,
            name="Challanger",
            game_id=KILLER_GAME_ID+1,
            tag=HOME_TAG,
            tournament_name=TOURNAMENT_NAME)

        victim = assign_to_team(
            self.session,
            name="Victim",
            game_id=KILLER_GAME_ID+2,
            tag=HOME_TAG,
            tournament_name=TOURNAMENT_NAME)

        teamkiller = assign_to_team(
            self.session,
            name="Teamkiller",
            game_id=KILLER_GAME_ID+3,
            tag=HOME_TAG,
            tournament_name=TOURNAMENT_NAME)

        add_kill(self.session, match, star, enemy, round=1)
        add_kill(self.session, match, challanger, enemy, round=2)
        add_kill(self.session, match, victim, enemy, round=3)
        add_kill(self.session, match, teamkiller, victim, round=4)

        add_assist(self.session, match, star, round=1)

        expected = PlayerEntry(star.name, K=1, D=0, TK=0, A=1)
        result = g.star_of_the_week(
            self.session, tour.tournament_name, week_number=1)

        self.assertEqual(expected, result)


if __name__ == '__main__':
    unittest.main()
