from mnbmodel.query import add
from mnbmodel.transactions.player_classes import get_classes, RANGED_CLASSES,\
    INFANTRY_CLASSES, CAVALRY_CLASSES
from .database_test_base import DatabaseTestSuite, setup_tournament,\
    add_match_player


class PlayerClassesInMatchTestSuite(DatabaseTestSuite):
    def setUp(self):
        DatabaseTestSuite.setUp(self)

    def test_no_match_player_entry(self):
        tour, _, _, match, killer, enemy = setup_tournament(self.session)

        result = get_classes(self.session, killer.name, match)
        self.assertEqual(set(), result)

    def test_get_classes_from_match_player_table(self):
        tour, home, away, match, killer, enemy = setup_tournament(self.session)
        other_match = add.match(self.session, home, away,
                                week_number=3, home_score=8, away_score=8)

        rng_class = next(iter(RANGED_CLASSES))
        inf_class = next(iter(INFANTRY_CLASSES))
        cav_class = next(iter(CAVALRY_CLASSES))
        another_cav_class = next(iter(CAVALRY_CLASSES))

        add_match_player(
            self.session, match, killer, spawn=1, round=1, cls=rng_class)
        add_match_player(
            self.session, match, killer, spawn=1, round=2, cls=inf_class)
        add_match_player(
            self.session, match, killer, spawn=1, round=2, cls=inf_class)
        add_match_player(
            self.session, match, killer, spawn=2, round=1, cls=cav_class)
        add_match_player(
            self.session, other_match, killer, spawn=2, round=1, cls=another_cav_class)

        expected = {rng_class, inf_class, cav_class}
        result = get_classes(self.session, killer.name, match)
        self.assertEqual(expected, result)
        self.assertFalse(cav_class != another_cav_class)
