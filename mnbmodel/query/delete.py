from sqlalchemy import and_
from .. import models as m


def match(session, home_team, away_team, week_number):
    _match = session.query(m.Match).filter(and_(
        m.Match.home_team == home_team,
        m.Match.away_team == away_team,
        m.Match.week_number == week_number)).first()

    if _match:
        session.delete(_match)
        session.flush()


def tournament(session, tour_name):
    _tour = session.query(m.Tournament).filter(
        m.Tournament.tournament_name == tour_name).first()

    if _tour:
        session.delete(_tour)
        session.flush()
