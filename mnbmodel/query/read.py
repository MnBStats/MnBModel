import collections
from sqlalchemy import and_, or_, func, distinct
from ..models import Tournament, TournamentTeam, Player, Match, GameID, Kill,\
    MatchPlayer, TournamentTeamPlayers


class TeamDoesNotExistError(Exception):
    """Raised when team is not found in database."""


def player(session, game_id):
    query = session.query(Player)
    query = query.filter(Player.game_ids.any(GameID.game_id == game_id))

    return query.one_or_none()


def player_from_name(session, player_name):
    query = session.query(Player)
    query = query.filter(Player.name == player_name)

    return query.one_or_none()


def team(session, tag, tournament_name):
    _tour = tournament(session, tournament_name)

    _team = session.query(TournamentTeam).filter(
        TournamentTeam.team_tag == tag,
        TournamentTeam.tournament_id == _tour.tournament_id).one_or_none()

    if _team is None:
        msg = "Team has not been found in database. Tag: {}, Tournament: {}"
        raise TeamDoesNotExistError(msg.format(tag, tournament_name))

    return _team


def team_from_name(session, team_name, tournament):
    _team = session.query(TournamentTeam).filter(
        TournamentTeam.team_name == team_name,
        TournamentTeam.tournament == tournament).one_or_none()

    if _team is None:
        msg = "Team has not been found in database. Team: {}, Tournament: {}"
        raise TeamDoesNotExistError(
            msg.format(team_name, tournament.tournament_name))

    return _team


def tournament(session, tournament_name):
    query = session.query(Tournament)
    query = query.filter(Tournament.tournament_name == tournament_name)

    return query.one()


def tournaments(session):
    return session.query(Tournament).all()


def player_team(session, game_id, tournament_name):
    _tour = tournament(session, tournament_name)
    _player = player(session, game_id)

    for _team in _player.teams:
        if _team.tournament_id == _tour.tournament_id:
            return _team


def tournament_game_ids(session, tournament_name):
    tour = tournament(session, tournament_name)

    query = session.query(Player.game_ids)
    query = query.join(Player.teams)
    query = query.filter(TournamentTeam.tournament_id == tour.tournament_id)
    return set(query)


def team_game_ids(session, tag, tournament_name):
    tour = tournament(session, tournament_name)

    query = session.query(Player.game_ids)
    query = query.join(Player.teams)
    query = query.filter(and_(
        TournamentTeam.tournament_id == tour.tournament_id,
        TournamentTeam.team_tag == tag))

    return set(query)


def num_of_weeks(session, tournament_name):
    query = session.query(func.count(distinct(Match.week_number)))
    query = query.filter(Match.tournament_id == Tournament.tournament_id)
    query = query.filter(Tournament.tournament_name == tournament_name)

    return query.scalar()


def num_of_rounds(session, match):
    return len(list(rounds(session, match)))


def match(session, tournament, week_number, team):
    query = session.query(Match)
    query = query.filter(Match.tournament == tournament)
    query = query.filter(or_(Match.home_team == team, Match.away_team == team))
    query = query.filter(Match.week_number == week_number)

    return query.one()


def matches(session, tour_name, week_number):
    query = session.query(Match)
    query = query.filter(Match.tournament_id == Tournament.tournament_id)
    query = query.filter(Tournament.tournament_name == tour_name)
    query = query.filter(Match.week_number == week_number)

    return query.all()


def players_in_match(session, match, team):
    query = session.query(Player)
    query = query.distinct(Player.name)

    query = query.outerjoin(
        TournamentTeamPlayers,
        TournamentTeamPlayers.c.player_id == Player.player_id)
    query = query.filter(
        TournamentTeamPlayers.c.team_id == team.tournament_team_id)

    query = query.filter(MatchPlayer.player_id == Player.player_id)
    query = query.filter(MatchPlayer.match == match)

    return query


def rounds(session, match):
    Round = collections.namedtuple('Round', ['spawn', 'round'])
    return (Round(spawn, round)
            for spawn in range(1, spawns_in_match(session, match)+1)
            for round in range(1, rounds_in_spawn(session, match, spawn)+1))


def spawns_in_match(session, match):
    spawns = session.query(func.count(distinct(MatchPlayer.spawn)))
    spawns = spawns.filter(MatchPlayer.match == match)
    return spawns.scalar()


def rounds_in_spawn(session, match, spawn):
    rounds = session.query(func.count(distinct(MatchPlayer.round)))
    rounds = rounds.filter(MatchPlayer.match == match)
    rounds = rounds.filter(MatchPlayer.spawn == spawn)
    return rounds.scalar()


def num_of_deaths_in_round(session, match):
    query = session.query(func.max(Kill.kill_number))
    query.filter(Kill.match == match)
    return query.scalar()


def num_of_rounds_played_in_match(session, match, name):
    query = session.query(func.count(MatchPlayer.match_player_id))
    query = query.filter(MatchPlayer.match == match)
    query = query.join(Player, Player.player_id == MatchPlayer.player_id)
    query = query.filter(Player.name == name)
    return query.scalar()
