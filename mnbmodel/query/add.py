from .. import models as m
from . import read
from .. import console_output

class DifferentTournamentTeamsError(Exception):
    """Raise when trying to add match with teams from different tournaments."""


def player(session, *, name, game_id):
    _player = m.Player()
    _player.name = name
    _player.game_ids.append(m.GameID(game_id=game_id))

    if read.player_from_name(session, name):
        console_output.name_exists(name)

    session.add(_player)
    session.flush()

    console_output.player_added(name, game_id)

    return _player


def match_player(session, match, player, side, wb_class, spawn, round):
    _mplayer = m.MatchPlayer(
        player=player,
        match=match,
        side=side,
        wb_class=wb_class,
        spawn=spawn,
        round=round)

    session.add(_mplayer)
    session.flush()

    return _mplayer


def team(session, name, tag, tournament_name):
    tour_id = session.query(m.Tournament.tournament_id).filter_by(
        tournament_name=tournament_name).scalar()

    _team = m.TournamentTeam(
        tournament_id=tour_id, team_name=name, team_tag=tag)

    session.add(_team)
    session.flush()

    return _team


def tournament(session, tour_name, full_name):
    tour = m.Tournament(
        tournament_name=tour_name,
        tournament_full_name=full_name)

    session.add(tour)
    session.flush()

    return tour


def tournament_rules(session, tour_name, spawns, rounds_to_win,
                     players_per_team):
    _tour = session.query(m.Tournament).filter(
        m.Tournament.tournament_name == tour_name).first()

    _tour.spawns = spawns
    _tour.rounds_to_win = rounds_to_win
    _tour.players_per_team = players_per_team

    session.add(_tour)
    session.flush()

    return _tour


def match(session, home_team, away_team, week_number, home_score, away_score):
    if home_team.tournament_id != away_team.tournament_id:
        raise DifferentTournamentTeamsError(
            "Match teams belong to different tournaments. {} ({}) vs {} ({})"
            .format(home_team.team_tag, home_team.tournament.tournament_name,
                    away_team.team_tag, away_team.tournament.tournament_name))

    _match = m.Match(
        tournament_id=home_team.tournament_id,
        week_number=week_number,
        home_team=home_team,
        away_team=away_team,
        home_team_score=home_score,
        away_team_score=away_score)

    session.add(_match)
    session.flush()

    return _match


def kill(session, *, match, killer, victim, kill_type, spawn, round,
         kill_number, weapon_id):

    tk = (match.home_team in killer.teams and match.home_team in victim.teams) \
        or (match.away_team in killer.teams and match.away_team in victim.teams)

    _kill = m.Kill(
        match=match,
        killer=killer,
        victim=victim,
        kill_type=kill_type,
        spawn=spawn,
        round=round,
        kill_number=kill_number,
        teamkill=tk,
        weapon_id=weapon_id)

    session.add(_kill)
    session.flush()

    return _kill


def assist(session, player, match, dmg, team_dmg, num_of_assists, spawn_number,
           round_number):

    _assist = m.Assist(
        match=match,
        player=player,
        dmg=dmg,
        team_dmg=team_dmg,
        number_of_assists=num_of_assists,
        spawn=spawn_number,
        round=round_number)

    session.add(_assist)
    session.flush()

    return _assist


def spawn(session, match, spawn_number, map, a_fac, a_team, a_score,
          d_score, d_team, d_fac, start_time):

    _spawn = m.Spawn(
        match=match,
        spawn=spawn_number,
        map=map,
        attacking_faction=a_fac,
        attacking_team=a_team,
        attacker_score=a_score,
        defender_score=d_score,
        defending_team=d_team,
        defending_faction=d_fac,
        start_time=start_time)

    session.add(_spawn)
    session.flush()

    return spawn
