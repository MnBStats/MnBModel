from ..query import read, add
from .. import console_output


class MulticlanningError(Exception):
    """Raised when trying to assign second team for the same tournament."""


def assign_to_team(session, name, game_id, tag, tournament_name):
    player = read.player(session, game_id)
    player = player or add.player(session, name=name, game_id=game_id)
    team = read.team(session, tag, tournament_name)

    assigned_team = read.player_team(session, game_id, tournament_name)

    if assigned_team and team != assigned_team:
        msg = "Player {} is assigned to different teams(teams={})."
        raise MulticlanningError(msg.format(
            player.name,
            assigned_team.team_name))

    if assigned_team is None:
        player.teams.append(team)
        console_output.player_assigned(team.team_name, name, game_id)

    return player
