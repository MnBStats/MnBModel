from sqlalchemy.sql.functions import coalesce
from ..models import Kill, MatchPlayer, TournamentTeamPlayers, Player
from ..query import read


class DeathInfo:
    def __init__(self, death_number=None, cls=None, weapon=None, enemy_cls=None):
        self.death_number = death_number
        self.cls = cls
        self.weapon = weapon
        self.enemy_cls = enemy_cls

    def __eq__(self, other):
        if not isinstance(other, DeathInfo):
            raise TypeError(other, type(other))
        return self.__dict__ == other.__dict__

    def __repr__(self):
        return ("<DeathInfo (DN: {}, CLS: {}, WPN: {}, ENM_CLS: {}>"
                .format(self.death_number, self.cls, self.weapon, self.enemy_cls))


def read_deaths_order_in_match(session, match, team, max_death):
    """
    Each player has a list of deaths numbers in consecutive rounds.

    Special values:
        NOT_PLAYED - player has not played
        max_death - player has survived the round
    """
    number_of_rounds = read.num_of_rounds(session, match)
    order = _init_dict(session, match, team, number_of_rounds)

    consecutive_round_number = 0
    for spawn, round in read.rounds(session, match):
        for player in _players_in_round(session, match, team, spawn, round):
            order[player.name][consecutive_round_number] = DeathInfo(
                death_number=player.DN if player.DN else max_death,
                cls=player.CLS,
                weapon=player.WPN,
                enemy_cls=player.ENM_CLS)

        consecutive_round_number += 1

    return order


def _init_dict(session, match, team, num_of_rounds):
    order = dict()
    for player in read.players_in_match(session, match, team):
        order[player.name] = [DeathInfo() for r in range(0, num_of_rounds)]

    return order


def _players_in_round(session, match, team, spawn, round):
    dn = session.query(
        Kill.victim_id.label('id'),
        Kill.killer_id.label('killer_id'),
        Kill.kill_number.label('DN'),
        Kill.weapon_id.label('WPN'),
        MatchPlayer.wb_class.label('ENM_CLS'))
    dn = dn.join(MatchPlayer, Kill.killer_id == MatchPlayer.player_id)
    dn = dn.filter(Kill.match == match)
    dn = dn.filter(Kill.spawn == spawn)
    dn = dn.filter(Kill.round == round)
    dn = dn.filter(MatchPlayer.match == match)
    dn = dn.filter(MatchPlayer.spawn == spawn)
    dn = dn.filter(MatchPlayer.round == round)
    dn = dn.subquery()

    query = session.query(
        MatchPlayer.player_id.label('id'),
        MatchPlayer.wb_class.label('CLS'),
        Player.name.label('name'),
        coalesce(dn.c.DN, None).label('DN'),
        coalesce(dn.c.WPN, None).label('WPN'),
        coalesce(dn.c.ENM_CLS, None).label('ENM_CLS'))

    query = query.join(Player, MatchPlayer.player_id == Player.player_id)
    query = query.outerjoin(dn, MatchPlayer.player_id == dn.c.id)

    query = query.outerjoin(
        TournamentTeamPlayers,
        TournamentTeamPlayers.c.player_id == Player.player_id)
    query = query.filter(
        TournamentTeamPlayers.c.team_id == team.tournament_team_id)

    query = query.filter(MatchPlayer.match == match)
    query = query.filter(MatchPlayer.spawn == spawn)
    query = query.filter(MatchPlayer.round == round)

    return query
