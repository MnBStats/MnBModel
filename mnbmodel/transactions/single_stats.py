from sqlalchemy import column
from ..models import Kill, Player
from .util.player_entry import PlayerEntries
from .util.filters import kill_type_filter, teamkill_filter
from .util.count import make_count


def headshots(session, tour_name, limit=None, filters=None):
    f = filters or []
    f.append(kill_type_filter(["<img=ico_headshot>"]))

    return PlayerEntries(
        _single_stat(
            session,
            tour_name,
            filters=f,
            limit=limit))


def couches(session, tour_name, limit=None, filters=None):
    f = filters or []
    f.append(kill_type_filter(["<img=ico_couchedlance>"]))

    return PlayerEntries(_single_stat(
            session,
            tour_name,
            filters=f,
            limit=limit))


def melee(session, tour_name, limit=None, filters=None):
    f = filters or []
    f.append(kill_type_filter(["<img=ico_axeone>", "<img=ico_swordone>"]))

    return PlayerEntries(
        _single_stat(
            session,
            tour_name,
            filters=f,
            limit=limit))


def hammer(session, tour_name, limit=None):
    return PlayerEntries(
        _single_stat(
            session,
            tour_name,
            filters=[kill_type_filter(["<img=ico_maul>"])],
            limit=limit))


def teamkillers(session, tour_name, limit=None):
    return PlayerEntries(
        _single_stat(
            session,
            tour_name,
            filters=[teamkill_filter(teamkill=True)],
            limit=limit))


def teamkilled(session, tour_name, limit=None):
    return PlayerEntries(
        _single_stat(
            session,
            tour_name,
            filters=[teamkill_filter(teamkill=True)],
            limit=limit,
            col=Kill.victim_id))


def _single_stat(session, tour_name, filters, limit=None, col=Kill.killer_id):
    kills = make_count(col, session, tour_name, filters).subquery()

    query = session.query(
        Player.name.label("name"),
        kills.c.count.label("K"))
    query = query.distinct(Player.name)
    query = query.filter(col == Player.player_id)
    query = query.filter(col == kills.c.col)
    query = query.order_by(column("K").desc())

    if limit is not None:
        query = query.limit(limit)

    return query
