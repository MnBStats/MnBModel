from sqlalchemy import func
from ..models import Kill, MatchPlayer, Player, TournamentTeam


RANGED_CLASSES = ['Archer', 'Crossbowman']
INFANTRY_CLASSES = ['Footman', 'Infantry', 'Spearman', 'Sergeant', 'Huscarl']
CAVALRY_CLASSES = ['Mamluke', 'Arms', 'Scout', 'Horseman']


class KillsPerClass:
    def __init__(self, inf, rng, cav):
        self.infantry = inf
        self.ranged = rng
        self.cavalry = cav

    def __eq__(self, other):
        if not isinstance(other, KillsPerClass):
            raise TypeError(other, type(other))
        return self.__dict__ == other.__dict__

    def __repr__(self):
        return ("<DeathInfo (inf: {}, rng: {}, cav: {}>"
                .format(self.infantry, self.ranged, self.cavalry))


def read_kills_per_class(session, match, team):
    return KillsPerClass(
        _kills_by_classes_in_match(session, match, team, INFANTRY_CLASSES),
        _kills_by_classes_in_match(session, match, team, RANGED_CLASSES),
        _kills_by_classes_in_match(session, match, team, CAVALRY_CLASSES))


def _kills_by_classes_in_match(session, match, team, classes):
    query = session.query(Kill.weapon_id, func.count(Kill.weapon_id))
    query = query.filter(Kill.teamkill == 0)
    query = query.filter(Kill.match == match)

    query = query.outerjoin(MatchPlayer, Kill.killer_id == MatchPlayer.player_id)
    query = query.filter(MatchPlayer.wb_class.in_(classes))
    query = query.filter(MatchPlayer.match == match)
    query = query.filter(MatchPlayer.spawn == Kill.spawn)
    query = query.filter(MatchPlayer.round == Kill.round)

    query = query.outerjoin(Player, Player.player_id == Kill.killer_id)
    query = query.filter(Player.teams.any(
        TournamentTeam.tournament_team_id == team.tournament_team_id))

    query = query.group_by(Kill.weapon_id)

    return query.all()
