from ... import models as m


def teamkill_filter(*, teamkill):
    def apply(query, *args, **kwargs):
        return query.filter_by(teamkill=teamkill)
    return apply


def kill_type_filter(kill_types):
    def apply(query, *args, **kwargs):
        return query.filter(m.Kill.kill_type.in_(kill_types))
    return apply


def match_filter(match):
    def apply(query, *args, **kwargs):
        return query.filter(kwargs['table'].match == match)
    return apply


def team_filter(team):
    def apply(query, *args, **kwargs):
        query = query.outerjoin(
            m.TournamentTeamPlayers,
            m.TournamentTeamPlayers.c.player_id == kwargs['id_type'])
        query = query.filter(
            m.TournamentTeamPlayers.c.team_id == team.tournament_team_id)
        return query
    return apply


def week_filter(week_number):
    def apply(query, *args, **kwargs):
        return query.filter(m.Match.week_number == week_number)
    return apply