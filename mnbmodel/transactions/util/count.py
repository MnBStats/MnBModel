from sqlalchemy import func
from ... import models as m


def make_count(col, session, tour_name, filters=None):
    filters = filters or []

    count = func.count(col).label("count")
    query = session.query(col.label("col"), count)

    query = query.filter(m.Kill.match_id == m.Match.match_id)
    query = query.filter(m.Match.tournament_id == m.Tournament.tournament_id)
    query = query.filter(m.Tournament.tournament_name == tour_name)
    query = query.group_by(col)

    for f in filters:
        query = f(query, id_type=col, table=m.Kill)

    return query
