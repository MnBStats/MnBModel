import itertools


class PlayerEntry:
    def __init__(self, name, **items):
        self.name = name
        self.items = items
        self.__dict__.update(**items)

    def __repr__(self):
        return 'PlayerEntry(name={}, {})'.format(self.name, self.items)

    def __eq__(self, other):
        if not isinstance(other, PlayerEntry):
            raise TypeError(other, type(other))
        return self.name == other.name and self.items == other.items

    def values(self):
        return self.items.values()


class PlayerEntries:
    def __init__(self, entries):
        self.entries = list(PlayerEntry(**row._asdict()) for row in entries)

    def __repr__(self):
        return ''.join([repr(entry) for entry in self.entries])

    def __getitem__(self, item):
        return self.entries[item]

    def __len__(self):
        return len(self.entries)

    def __eq__(self, other):
        if not isinstance(other, PlayerEntries):
            raise TypeError(other, type(other))
        for entry, other_entry in itertools.zip_longest(self.entries,
                                                        other.entries):
            if entry != other_entry:
                return False
        return True

    def first(self):
        return self.entries[0]
