from sqlalchemy import or_, func, column
from sqlalchemy.sql.functions import coalesce
from .. import models as MDL
from .util.count import make_count
from .util.player_entry import PlayerEntries
from .util.filters import \
    team_filter, match_filter, week_filter, teamkill_filter

Tournament = MDL.Tournament
Team = MDL.TournamentTeam
Player = MDL.Player
GameId = MDL.GameID
Match = MDL.Match
Kill = MDL.Kill
Assist = MDL.Assist
TeamPlayers = MDL.TournamentTeamPlayers


def star_of_the_week(session, tour_name, week_number):
    """
    Function selects player of the week with stats.
    """
    return PlayerEntries(_select_stats(
        session,
        tour_name,
        filters=[week_filter(week_number)],
        limit=1
    )).first()


def team_stats(session, match, team):
    """
    Function selects player stats for a given team within a match.
    """
    return PlayerEntries(_select_stats(
        session,
        match.tournament.tournament_name,
        filters=[team_filter(team), match_filter(match)]
    ))


def general_stats(session, tour_name, limit=None):
    """
    Function queries general stats from one tournament.
    It can be limited to given amount of entries.
    We need union or players with 0 deaths won't be selected.
    """
    return PlayerEntries(_select_stats(session, tour_name, limit))


def _select_stats(session, tour_name, limit=None, filters=None):
    stats_per_killer_id = _select_stats_union(
        session, tour_name, Kill.killer_id, filters)
    stats_per_victim_id = _select_stats_union(
        session, tour_name, Kill.victim_id, filters)

    stats = stats_per_killer_id.union(stats_per_victim_id)

    stats = stats.order_by(
        column("K").desc(),
        column("A").desc(),
        column("TK").asc(),
        column("D").asc())

    if limit is not None:
        stats = stats.limit(limit)

    return stats


def _select_stats_union(session, tour_name, id_type, filters=None):
    kills = kills_count(session, tour_name, filters).subquery()
    deaths = deaths_count(session, tour_name, filters).subquery()
    tks = teamkills_count(session, tour_name, filters).subquery()
    assists = assists_sum(session, tour_name, filters).subquery()

    query = session.query(
        Player.name.label("name"),
        coalesce(kills.c.count, 0).label("K"),
        coalesce(deaths.c.count, 0).label("D"),
        coalesce(tks.c.count, 0).label("TK"),
        coalesce(assists.c.num, 0).label("A"))
    query = query.distinct(Player.name)
    query = query.outerjoin(Kill, id_type == Player.player_id)
    query = query.outerjoin(kills, kills.c.col == id_type)
    query = query.outerjoin(deaths, deaths.c.col == id_type)
    query = query.outerjoin(tks, tks.c.col == id_type)
    query = query.outerjoin(assists, assists.c.col == id_type)
    query = query.filter(or_(
        kills.c.count > 0,
        assists.c.num > 0,
        deaths.c.count > 0,
        tks.c.count > 0))

    return query


def kills_count(session, tour_name, filters=None):
    f = list(filters or [])
    f.insert(0, teamkill_filter(teamkill=False))

    return make_count(
        Kill.killer_id,
        session,
        tour_name,
        filters=f)


def deaths_count(session, tour_name, filters=None):
    f = list(filters or [])
    return make_count(
        Kill.victim_id,
        session,
        tour_name,
        filters=f)


def teamkills_count(session, tour_name, filters=None):
    f = list(filters or [])
    f.insert(0, teamkill_filter(teamkill=True))

    return make_count(
        Kill.killer_id,
        session,
        tour_name,
        filters=f)


def assists_sum(session, tour_name, filters=None):
    filters = list(filters or [])

    sum = func.sum(Assist.number_of_assists).label("num")
    query = session.query(Assist.player_id.label("col"), sum)

    query = query.filter(Assist.match_id == Match.match_id)
    query = query.filter(Match.tournament_id == Tournament.tournament_id)
    query = query.filter(Tournament.tournament_name == tour_name)
    query = query.group_by(Assist.player_id)

    for f in filters:
        query = f(query, id_type=Assist.player_id, table=Assist)

    return query
