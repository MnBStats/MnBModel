from sqlalchemy import distinct
from mnbmodel.models import MatchPlayer, Player

RANGED_CLASSES = {'Archer', 'Crossbowman'}
INFANTRY_CLASSES = {'Footman', 'Infantry', 'Spearman', 'Sergeant', 'Huscarl'}
CAVALRY_CLASSES = {'Mamluke', 'Arms', 'Scout', 'Horseman'}


def get_classes(session, name, match):
    query = session.query(distinct(MatchPlayer.wb_class))\
        .join(Player, Player.player_id == MatchPlayer.player_id)\
        .filter(Player.name == name)\
        .filter(MatchPlayer.match == match)

    return set(r for r, in query.all())
