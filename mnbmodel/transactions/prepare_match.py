from ..query import read, delete, add


def prepare_match(session, tournament_name, week_number, home_tag, away_tag,
                  home_score, away_score):
    home_team = read.team(session, home_tag, tournament_name)
    away_team = read.team(session, away_tag, tournament_name)

    delete.match(session, home_team, away_team, week_number)
    return add.match(session, home_team, away_team, week_number, home_score, away_score)
