from sqlalchemy import Column, Date, DateTime, ForeignKey, Index, Integer, \
    String, Table, UniqueConstraint
from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()
metadata = Base.metadata


class GameID(Base):
    __tablename__ = 'GameIDs'

    game_id = Column(Integer, primary_key=True)
    player_id = Column(ForeignKey('Players.player_id'), nullable=False, index=True)

    player = relationship('Player')


class Player(Base):
    __tablename__ = 'Players'
    player_id = Column(Integer, primary_key=True)
    name = Column('name', String(45), nullable=False)
    teams = relationship('TournamentTeam', secondary='TournamentTeamPlayers')
    game_ids = relationship('GameID')


TournamentTeamPlayers = Table(
    'TournamentTeamPlayers', metadata,
    Column('team_id', ForeignKey('TournamentTeams.tournament_team_id'), primary_key=True, nullable=False),
    Column('player_id', ForeignKey('Players.player_id'), primary_key=True, nullable=False, index=True)
)


class TournamentTeam(Base):
    __tablename__ = 'TournamentTeams'

    tournament_team_id = Column(Integer, primary_key=True)
    tournament_id = Column(ForeignKey('Tournaments.tournament_id'), nullable=False, index=True)
    team_name = Column(String(45), nullable=False)
    team_tag = Column(String(45), nullable=False)

    players = relationship('Player', secondary='TournamentTeamPlayers')
    tournament = relationship('Tournament', backref=backref("teams", cascade="all,delete"))


class Tournament(Base):
    __tablename__ = 'Tournaments'
    __table_args__ = (
        UniqueConstraint('tournament_name', name='TourNameUnique'),
        UniqueConstraint('tournament_full_name', name='TourFullNameUnique'),
    )

    tournament_id = Column(Integer, primary_key=True)
    tournament_name = Column(String(45), nullable=False)
    tournament_full_name = Column(String(45), nullable=False)
    start_date = Column(Date)
    end_date = Column(Date)
    spawns = Column(Integer, nullable=False, default=0)
    rounds_to_win = Column(Integer, nullable=False, default=0)
    players_per_team = Column(Integer, nullable=False, default=0)


class User(Base):
    __tablename__ = 'Users'

    user_id = Column(Integer, primary_key=True)
    username = Column(String(45), nullable=False)
    password = Column(String(45), nullable=False)


class Week(Base):
    __tablename__ = 'Weeks'

    week_id = Column(Integer, primary_key=True)
    first_map = Column(String(45))
    faction_a = Column(String(45))
    faction_b = Column(String(45))
    second_map = Column(String(45))
    faction_c = Column(String(45))
    faction_d = Column(String(45))


t_schema_migrations = Table(
    'schema_migrations', metadata,
    Column('version', String(85), nullable=False, unique=True)
)


class Match(Base):
    __tablename__ = 'Matches'
    __table_args__ = (
        Index('match_is_unique', 'tournament_id', 'week_number', 'home_team_id', 'away_team_id', unique=True),
    )

    match_id = Column(Integer, primary_key=True)
    tournament_id = Column(ForeignKey('Tournaments.tournament_id'), nullable=False, index=True)
    week_number = Column(Integer, nullable=False)
    home_team_id = Column(ForeignKey('TournamentTeams.tournament_team_id'), nullable=False, index=True)
    away_team_id = Column(ForeignKey('TournamentTeams.tournament_team_id'), nullable=False, index=True)
    home_team_score = Column(Integer)
    away_team_score = Column(Integer)
    date = Column(DateTime)

    away_team = relationship('TournamentTeam', primaryjoin='Match.away_team_id == TournamentTeam.tournament_team_id')
    home_team = relationship('TournamentTeam', primaryjoin='Match.home_team_id == TournamentTeam.tournament_team_id')
    tournament = relationship(Tournament, backref=backref("matches", cascade="all,delete"))


class Kill(Base):
    __tablename__ = 'Kills'
    __table_args__ = (
        Index('kill_is_unique', 'round', 'kill_number', 'match_id', 'spawn', unique=True),
    )

    kill_id = Column(Integer, primary_key=True)
    match_id = Column(Integer, ForeignKey(Match.match_id, ondelete='CASCADE'), nullable=False)
    killer_id = Column(ForeignKey('Players.player_id'), ForeignKey('TournamentTeamPlayers.player_id'), nullable=False, index=True)
    victim_id = Column(ForeignKey('Players.player_id'), nullable=False, index=True)
    kill_type = Column(String(45), nullable=False)
    spawn = Column(Integer, nullable=False)
    round = Column(Integer, nullable=False)
    kill_number = Column(Integer, nullable=False)
    teamkill = Column(Integer, nullable=False)
    weapon_id = Column(String(65))

    killer = relationship('Player', primaryjoin='Kill.killer_id == Player.player_id')
    victim = relationship('Player', primaryjoin='Kill.victim_id == Player.player_id')
    match = relationship(Match, backref=backref("kills", cascade="all,delete"))


class Assist(Base):
    __tablename__ = 'Assists'

    assist_id = Column(Integer, primary_key=True)
    match_id = Column(ForeignKey('Matches.match_id', ondelete='CASCADE'), nullable=False, index=True)
    player_id = Column(ForeignKey('Players.player_id'), nullable=False, index=True)
    dmg = Column(Integer, nullable=False)
    team_dmg = Column(Integer, nullable=False)
    number_of_assists = Column(Integer, nullable=False)
    spawn = Column(Integer, nullable=False)
    round = Column(Integer, nullable=False)

    player = relationship(
        'Player', primaryjoin='Assist.player_id == Player.player_id')
    match = relationship(Match, backref=backref("assists", cascade="all,delete"))


class MatchPlayer(Base):
    __tablename__ = 'MatchPlayers'

    match_player_id = Column(Integer, primary_key=True)
    match_id = Column(ForeignKey('Matches.match_id', ondelete='CASCADE'), nullable=False, index=True)
    player_id = Column(ForeignKey('Players.player_id'), nullable=False, index=True)
    side = Column(String(45), nullable=False)
    wb_class = Column(String(45), nullable=False)
    spawn = Column(Integer, nullable=False)
    round = Column(Integer, nullable=False)

    player = relationship(
        'Player', primaryjoin='MatchPlayer.player_id == Player.player_id')
    match = relationship(Match, backref=backref("match_players", cascade="all,delete"))


class Spawn(Base):
    __tablename__ = 'Spawns'

    match_id = Column(ForeignKey('Matches.match_id', ondelete='CASCADE'), primary_key=True, nullable=False, index=True)
    spawn = Column(Integer, primary_key=True, nullable=False, index=True)
    map = Column(String(45), nullable=False)
    attacking_faction = Column(String(45), nullable=False)
    attacking_team = Column(Integer, nullable=False)
    attacker_score = Column(Integer, nullable=False)
    defender_score = Column(Integer, nullable=False)
    defending_team = Column(Integer, nullable=False)
    defending_faction = Column(String(45), nullable=False)
    start_time = Column(String(8), nullable=False)

    match = relationship(Match, backref=backref("match_spawns", cascade="all,delete"))
