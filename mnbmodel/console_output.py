from colorama import Fore, Back


def player_assigned(team_name, name, game_id):
    color = Fore.CYAN
    print(color + "{} ({}) assigned to {}.".format(
        name,
        str(game_id),
        team_name))


def name_exists(name):
    color = Fore.RED
    print(color + "Player with name {} already exists in the database.".format(
        name))


def player_added(name, game_id):
    color = Fore.GREEN
    print(color + "{} ({}) added to the database.".format(
        name,
        str(game_id)))
