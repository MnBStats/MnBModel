import contextlib
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from .models import Base


class Database:
    def __init__(self, conn_string=None):
        self.conn_string = conn_string or 'sqlite:///app.db'
        self.engine = create_engine(self.conn_string)
        Base.metadata.create_all(self.engine)
        self.Session = sessionmaker(bind=self.engine)

    def connect(self):
        return self.Session()


@contextlib.contextmanager
def session_scope(db):
    """Provide a transactional scope around a series of operations."""
    session = db.connect()
    try:
        yield session
        session.commit()
    except:
        session.rollback()
        raise
    finally:
        session.close()
