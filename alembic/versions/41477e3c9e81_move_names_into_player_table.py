"""move names into player table

Revision ID: 41477e3c9e81
Revises: cfab2a4f352d
Create Date: 2019-01-02 11:34:03.282848

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.orm import relationship
from sqlalchemy.orm.session import Session
from sqlalchemy.ext.declarative import declarative_base
from mnbmodel.models import TournamentTeamPlayers

# revision identifiers, used by Alembic.
revision = '41477e3c9e81'
down_revision = 'cfab2a4f352d'
branch_labels = None
depends_on = None

Base = declarative_base()


class Player(Base):
    __tablename__ = 'Players'
    player_id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column('name', sa.String(45), nullable=False)
    names = relationship('Name')


class Name(Base):
    __tablename__ = 'Names'
    name = sa.Column(sa.String(45), primary_key=True, nullable=False)
    player_id = sa.Column(sa.ForeignKey('Players.player_id'), primary_key=True, nullable=False, index=True)
    player = relationship('Player')


def upgrade():
    bind = op.get_bind()

    with op.batch_alter_table('Players') as batch_op:
        batch_op.add_column(sa.Column('name', sa.String(45), nullable=True))

    session = Session(bind=bind)
    for player in session.query(Player):
        player.name = player.names[0].name

    session.commit()

    with op.batch_alter_table('Players') as batch_op:
        batch_op.alter_column('name', nullable=False)

    op.drop_table('Names')


def downgrade():
    bind = op.get_bind()
    Name.__table__.create(bind)

    session = Session(bind=bind)
    for player in session.query(Player):
        name = Name()
        name.name = player.name
        name.player_id = player.player_id

        player.names.append(name)

    session.commit()

    with op.batch_alter_table('Players') as batch_op:
        batch_op.drop_column('name')
